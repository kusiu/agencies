<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Agency
 *
 * @ORM\Table(name="agency")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AgencyRepository")
 */
class Agency
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="Recruiter", mappedBy="agency")
     */
    private $recruiters;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Agency
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->recruiters = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add recruiter
     *
     * @param \AppBundle\Entity\Recruiter $recruiter
     *
     * @return Agency
     */
    public function addRecruiter(\AppBundle\Entity\Recruiter $recruiter)
    {
        $this->recruiters[] = $recruiter;

        return $this;
    }

    /**
     * Remove recruiter
     *
     * @param \AppBundle\Entity\Recruiter $recruiter
     */
    public function removeRecruiter(\AppBundle\Entity\Recruiter $recruiter)
    {
        $this->recruiters->removeElement($recruiter);
    }

    /**
     * Get recruiters
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRecruiters()
    {
        return $this->recruiters;
    }
}
